<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 22.02.18
 * Time: 20:42
 */

namespace Vitd\FixImageModule\Magento_Framework;

class Image extends \Magento\Framework\Image
{

    public function getResizedImageInfo() {
        return $this->_adapter->getResizedImageInfo();
    }

}