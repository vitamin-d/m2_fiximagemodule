<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 15.02.18
 * Time: 21:18
 */

namespace Vitd\FixImageModule\Magento_Framework\Image\Adapter;

class Gd2 extends \Magento\Framework\Image\Adapter\Gd2
{

    /**
     * Reassign image dimensions
     *
     * @return object [ width, height ]
     */
    public function refreshImageDimensions()
    {
        $this->_imageSrcWidth = imagesx($this->_imageHandler);
        $this->_imageSrcHeight = imagesy($this->_imageHandler);

        return (object) [
          'width' => $this->_imageSrcWidth,
          'height' => $this->_imageSrcHeight,
        ];
    }

    public function getResizedImageInfo() {

        return [
            "0" => imagesx($this->_imageHandler),
            "1" => imagesy($this->_imageHandler),
            "2" => $this->getImageType(),
            "3" => 'height="'.imagesx($this->_imageHandler).'" width="'.imagesy($this->_imageHandler).'"',
            "channels" => 0,
            "bits" => 0,
            "mime" => $this->getMimeType()
        ];

    }

}