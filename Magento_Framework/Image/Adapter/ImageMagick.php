<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 15.02.18
 * Time: 21:23
 */

namespace Vitd\FixImageModule\Magento_Framework\Image\Adapter;

class ImageMagick extends \Magento\Framework\Image\Adapter\ImageMagick
{

    /**
     * Reassign image dimensions
     *
     * @return object [ width, height ]
     */
    public function refreshImageDimensions()
    {
        $this->_imageSrcWidth = $this->_imageHandler->getImageWidth();
        $this->_imageSrcHeight = $this->_imageHandler->getImageHeight();
        $this->_imageHandler->setImagePage($this->_imageSrcWidth, $this->_imageSrcHeight, 0, 0);

        return (object) [
            'width' => $this->_imageSrcWidth,
            'height' => $this->_imageSrcHeight,
        ];
    }

    public function getResizedImageInfo() {

        return [
            "0" => $this->_imageHandler->getImageWidth(),
            "1" => $this->_imageHandler->getImageHeight(),
            "2" => $this->getImageType(),
            "3" => 'height="'.$this->_imageHandler->getImageWidth().'" width="'.$this->_imageHandler->getImageHeight().'"',
            "channels" => 0,
            "bits" => 0,
            "mime" => $this->getMimeType()
        ];

    }

}