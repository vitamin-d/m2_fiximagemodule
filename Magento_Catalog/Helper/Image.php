<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 22.02.18
 * Time: 10:40
 */

namespace Vitd\FixImageModule\Magento_Catalog\Helper;



class Image extends \Magento\Catalog\Helper\Image
{

    /**
     * Scheduled for rotate image
     *
     * @var bool
     */
    protected $_scheduleCrop = false;


    /**
     * @return $this
     */
    protected function _reset()
    {
        parent::_reset();
        $this->_scheduleCrop = false;
        return $this;
    }

    /**
     * Crop image into specified angle
     *
     * @param number $top
     * @param number $left
     * @param number $right
     * @param number $bottom
     * @return $this
     */
    public function crop($top, $left = null, $right = null, $bottom = null)
    {
        /** @var \Vitd\FixImageModule\Magento_Catalog\Model\Product\Image $model */
        $model = $this->_getModel();
        $model->setTop($top)->setLeft($left)->setRight($right)->setBottom($bottom);
        $this->_scheduleCrop = true;
        return $this;
    }


    /**
     * Set image properties
     *
     * @return $this
     */
    protected function setImageProperties()
    {
        parent::setImageProperties();

        /** @var \Vitd\FixImageModule\Magento_Catalog\Model\Product\Image $model */
        $model = $this->_getModel();

        // Auto enable cropping
        $frame = $this->getFrame() ?: 'true';
        $aspectRatio = $this->getAttribute('aspect_ratio') ?: 'true';

        if (!empty($frame) && !empty($aspectRatio)) {
            $this->_scheduleCrop = $frame === 'false' && $aspectRatio === 'true';
            $model->setEnableCropping($this->_scheduleCrop);
        }

        return $this;
    }

    /**
     * Apply scheduled actions
     *
     * @return $this
     * @throws \Exception
     */
    protected function applyScheduledActions()
    {
        $this->initBaseFile();
        if ($this->isScheduledActionsAllowed()) {

            /** @var \Vitd\FixImageModule\Magento_Catalog\Model\Product\Image $model */
            $model = $this->_getModel();
            if ($this->_scheduleRotate) {
                $model->rotate($this->getAngle());
            }
            if ($this->_scheduleResize) {
                $model->resize();
            }
            if ($this->_scheduleCrop) {
                $model->crop();
            }
            if ($this->getWatermark()) {
                $model->setWatermark($this->getWatermark());
            }
            $model->saveFile();

        }
        return $this;
    }


}