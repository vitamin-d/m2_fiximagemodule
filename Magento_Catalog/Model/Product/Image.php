<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 22.02.18
 * Time: 11:42
 */

namespace Vitd\FixImageModule\Magento_Catalog\Model\Product;

class Image extends \Magento\Catalog\Model\Product\Image
{

    /**
     * @var int
     */
    protected $_targetWidth = 0;

    /**
     * @var int
     */
    protected $_targetHeight = 0;

    /**
     * @var int
     */
    protected $_top;

    /**
     * @var int
     */
    protected $_left;

    /**
     * @var int
     */
    protected $_right;

    /**
     * @var int
     */
    protected $_bottom;

    /**
     * @var bool
     */
    protected $_enable_cropping = false;


    /**
     * @var \Magento\Catalog\Model\View\Asset\ImageFactory
     */
    private $viewAssetImageFactory;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig,
        \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\Factory $imageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\View\FileSystem $viewFileSystem,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [],
        \Magento\Catalog\Model\View\Asset\ImageFactory $viewAssetImageFactory = null,
        \Magento\Catalog\Model\View\Asset\PlaceholderFactory $viewAssetPlaceholderFactory = null
    )
    {
        parent::__construct(
            $context,
            $registry,
            $storeManager,
            $catalogProductMediaConfig,
            $coreFileStorageDatabase,
            $filesystem,
            $imageFactory,
            $assetRepo,
            $viewFileSystem,
            $scopeConfig,
            $resource,
            $resourceCollection,
            $data,
            $viewAssetImageFactory,
            $viewAssetPlaceholderFactory
        );

        $this->viewAssetImageFactory = $viewAssetImageFactory ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Catalog\Model\View\Asset\ImageFactory::class);
    }


    public function setEnableCropping($enabled)
    {
        $this->_enable_cropping = $enabled === true || $enabled === 'true';
        $this->_targetWidth = $this->_width;
        $this->_targetHeight = $this->_height;
        return $this;
    }


    /**
     * @param int $top
     * @return $this
     */
    public function setTop($top)
    {
        $this->_top = $top ?: 0;
        return $this;
    }

    /**
     * @return int
     */
    public function getTop()
    {
        return $this->_top ?: 0;
    }

    /**
     * @param int $left
     * @return $this
     */
    public function setLeft($left)
    {
        $this->_left = $left ?: 0;
        return $this;
    }

    /**
     * @return int
     */
    public function getLeft()
    {
        return $this->_left ?: 0;
    }

    /**
     * @param int $right
     * @return $this
     */
    public function setRight($right)
    {
        $this->_right = $right ?: 0;
        return $this;
    }

    /**
     * @return int
     */
    public function getRight()
    {
        return $this->_right ?: 0;
    }

    /**
     * @param int $bottom
     * @return $this
     */
    public function setBottom($bottom)
    {
        $this->_bottom = $bottom ?: 0;
        return $this;
    }

    /**
     * @return int
     */
    public function getBottom()
    {
        return $this->_bottom ?: 0;
    }

    public function isCroppingEnabled()
    {
        return ($this->getTop() | $this->getLeft() | $this->getRight() | $this->getBottom()) > 0 || $this->_enable_cropping;
    }

    /**
     * Get image File Info from Resized, Cached or Original
     */
    public function getImageInfo()
    {
        $fileInfo = null;

        # Get resized file info
        if (empty($fileInfo) && !empty($this->getImageProcessor())) {
            if (method_exists($this->getImageProcessor(), 'getResizedImageInfo')) {
                $fileInfo = $this->getImageProcessor()->getResizedImageInfo();
            }
        }

        # Get cached file info
        if (empty($fileInfo) && $this->isCached()) {
            $fileInfo = $fileInfo = $this->getResizedImageInfo();
        }

        # Get original file info
        if (empty($fileInfo)) {
            if ($this->_mediaDirectory->isExist($this->getBaseFile())) {
                $image = $this->_mediaDirectory->getAbsolutePath($this->getBaseFile());
                $fileInfo = getimagesize($image);
            }
        }

        return $fileInfo;
    }


    /**
     * @see \Magento\Framework\Image\Adapter\AbstractAdapter
     * @return $this
     */
    public function resize()
    {
        if ($this->getWidth() === null && $this->getHeight() === null) {
            return $this;
        }

        $this->_targetWidth = $this->_width;
        $this->_targetHeight = $this->_height;

        $toCropping = $this->isCroppingEnabled();
        if ($toCropping) {
            $_ratio_target = $this->_width / $this->_height;

            if ($imageSize = $this->getImageInfo()) {
                $_ratio_image = $imageSize[0] / $imageSize[1];
            } else {
                $_ratio_image = $_ratio_target;
            }

            if ($_ratio_image !== $_ratio_target) {
                if ($_ratio_image > $_ratio_target) {
                    # >1: incrase width
                    $scale = $_ratio_image / $_ratio_target;
                    $this->_width = $this->_width * $scale;
                }
                elseif ($_ratio_image < $_ratio_target) {
                    # <1: incrase height
                    $scale = $_ratio_target / $_ratio_image;
                    $this->_height = $this->_height * $scale;
                }
            }

            $this->_width += $this->getLeft() + $this->getRight();
            $this->_height += $this->getTop() + $this->getBottom();
        }

        $this->getImageProcessor()->resize($this->_width, $this->_height);
        return $this;
    }


    /**
     * @see \Magento\Framework\Image\Adapter\AbstractAdapter
     * @return $this
     */
    public function crop()
    {
        if (!$this->isCroppingEnabled()) {
            return $this;
        }

        if ($imageSize = $this->getImageInfo()) {
            if (
                ($this->_targetWidth === $imageSize[0] && $this->_targetHeight === $imageSize[1]) ||
                empty($this->_targetWidth) ||
                empty($this->_targetHeight)
            ) {
                $this->_enable_cropping = false;
                return $this;
            }

            $dWidth = $imageSize[0] - $this->_targetWidth - $this->getLeft() - $this->getRight();
            $dHeight = $imageSize[1] - $this->_targetHeight - $this->getTop() - $this->getBottom();

            if ($dWidth < 0 || $dHeight < 0) {
                return $this;
            }

            $dLeftRight = ($dWidth - $this->getLeft() - $this->getRight()) / 2;
            $dTopBottom = ($dHeight - $this->getTop() - $this->getBottom()) / 2;
            $this->setLeft($this->getLeft() + $dLeftRight);
            $this->setRight($this->getRight() + $dLeftRight);
            $this->setTop($this->getTop() + $dTopBottom);
            $this->setBottom($this->getBottom() + $dTopBottom);
        }


        if ($this->isCroppingEnabled()) {
            $this->getImageProcessor()->crop($this->getTop(), $this->getLeft(), $this->getRight(), $this->getBottom());
        }

        return $this;
    }

}